package com.example.emploi;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ListGroupe extends Fragment {

    DbHelper db ;
    java.util.Calendar calendar = java.util.Calendar.getInstance();

    int year ;
    int month;
    int dayOfMonth;

    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    //list of courses
    public List<Cours> listCours = new ArrayList<>();
    private RecyclerView recyclerView;
    private CoursAdapter mAdapter;
    TextView noCourses;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    protected View mView;


    public ListGroupe() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters
    public static ListGroupe newInstance(String param1, String param2) {
        ListGroupe fragment = new ListGroupe();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.list_groupe, container, false);
        this.mView = view;
        db = new DbHelper(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        noCourses =(TextView) view.findViewById(R.id.no_course);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        year = calendar.get(java.util.Calendar.YEAR);
        month = calendar.get(java.util.Calendar.MONTH);
        dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);
        getDayPerDate(year, month, dayOfMonth);


        Button btnSelect = view.findViewById(R.id.date_select);

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                java.util.Calendar calendar = java.util.Calendar.getInstance();
                int year = calendar.get(java.util.Calendar.YEAR);
                int month = calendar.get(java.util.Calendar.MONTH);
                int dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                getDayPerDate(year, month, day);
                            }


                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        return view;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/tdoption/3390,3391,3392,3393,3394,3395,28400,28399,28401");

    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void getDayPerDate(int year, int month, int day) {

        ArrayList<String> grps = getGrpsSharedPrefs();

        noCourses.setVisibility(View.INVISIBLE);
        String dayString ;
        String monthString;

        //le sélecteur de date renvoie le mauvais mois
        if(month < 10)
            monthString = "0" + (month+1);
        else
            monthString = ""+(month+1);


        if(day < 10)
            dayString  = "0" + day ;
        else
            dayString = ""+day;


        try {
            listCours = db.getAllCourses(year+"-"+monthString+"-"+dayString,grps);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        if(listCours.size()==0)
            noCourses.setVisibility(View.VISIBLE);

        mAdapter = new CoursAdapter(listCours,"courses");
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        System.out.print(listCours.toString());
    }

    public ArrayList<String> getGrpsSharedPrefs(){
        ArrayList<String> grps = new ArrayList<String>();

        SharedPreferences prefs = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String td1 = prefs.getString("td1_selected", null);

        String td2 = prefs.getString("td2_selected", null);

        String td3 = prefs.getString("td3_selected", null);

        String td4 = prefs.getString("td4_selected", null);

        String td5 = prefs.getString("td5_selected", null);

        String td6 = prefs.getString("td6_selected", null);

        String td7 = prefs.getString("td7_selected", null);

        String td8 = prefs.getString("td8_selected", null);

        String td9 = prefs.getString("td9_selected", null);

        grps.add(td1);
        grps.add(td2);
        grps.add(td3);
        grps.add(td4);
        grps.add(td5);
        grps.add(td6);
        grps.add(td7);
        grps.add(td8);
        grps.add(td9);

        return grps;
    }

}
