package com.example.emploi;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.util.MapTimeZoneCache;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListCours.OnFragmentInteractionListener,
        ListGroupe.OnFragmentInteractionListener, Edition.OnFragmentInteractionListener,
        Exam.OnFragmentInteractionListener{
    Fragment fragment = null;

    DbHelper db ;
    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    ProgressBar progressBar;



    @Override
    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("EDT");
        db = new DbHelper(this);


        System.setProperty("net.fortuna.ical4j.timezone.cache.impl", MapTimeZoneCache.class.getName());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        navigationView.setNavigationItemSelectedListener(this);

        //set default fragment
        SharedPreferences prefs = getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String isFirst = prefs.getString("first_lance", "true");
        if(isFirst.equals("false")){
            fragment = new ListCours();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();
        }

        else {
            fragment = new Edition();
            ((Edition) fragment).setFirstLunch(true);
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

           /* AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Bienvenue");
            alertDialog.setMessage("Bonjour,\n" +
                    "apparemment c'est votre premier lancement de l'application, " +
                    "par consequence vous  êtes digérés automatiquement vers la page de configuration.\n" +
                    "Merci d'entrer le lien vers votre EDT (assurez-vous qu'il est juste 100%), aussi de sélectionner votre groupe(s)  du TD.\n" +
                    "Par suite cliquez sur mettre-a-jour donnéés dans le menu paramertres.\n \n"+
                    "");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();*/
        }




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // ajout d element a  action bar sil exist
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            SharedPreferences prefs = getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);
            String url_saved = prefs.getString("url_edt", "Default");

            //verifier la connexion d'internet
            if(isInternetConnection()) {
                // executeUpdateThread(/*url_saved*/"https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_ADA");
                executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_BLAISE");

            }else {
                CharSequence text = "Vérifiez votre connexion Internet !";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_camera) {
            fragment = new ListCours();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();
        } else if (id == R.id.nav_gallery) {

            fragment = new ListGroupe();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        } else if (id == R.id.nav_manage) {

            fragment = new Edition();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        } else if (id == R.id.next_quez) {

            fragment = new Exam();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }





    public void executeUpdateThread(String url){

        MainActivity.Update update = new MainActivity.Update();
        update.execute(url);

    }

    public class Update extends AsyncTask<String, Void, net.fortuna.ical4j.model.Calendar> {


        protected net.fortuna.ical4j.model.Calendar doInBackground (String...urls){
            net.fortuna.ical4j.model.Calendar calendar = null;
            URLConnection urlConnection = null;

            try {
                URL url = new URL(urls[0]);
                urlConnection = url.openConnection();
                Log.d(" connect :", "Ue");

                // urlConnection.connect();
                InputStream inputStream = urlConnection.getInputStream();
                Log.d("inputStream :", inputStream.toString());
                CalendarBuilder builder = new CalendarBuilder();
                //bibliotheque ajouté dans le graedle
                calendar = builder.build(inputStream);

            } catch (Exception e) {
                Log.d("connect :", "er");
                e.printStackTrace();
            } finally {
            }

            return calendar;
        }

        @Override
        protected void onPostExecute (Calendar calendar){
            for (Iterator i = calendar.getComponents().iterator(); i.hasNext(); ) {
                Component component = (Component) i.next();
                if (component.getName().equalsIgnoreCase("VEVENT")) {
                    calendarEntry = new HashMap<>();
                    for (Iterator j = component.getProperties().iterator(); j.hasNext(); ) {
                        net.fortuna.ical4j.model.Property property = (Property) j.next();
                        calendarEntry.put(property.getName(), property.getValue());
                    }
                    calendarEntries.add(calendarEntry);
                }

            }
            db.dropTable();
            try {
                db.addCourses(calendarEntries);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.INVISIBLE);

            super.onPostExecute(calendar);
        }
    }

    public  boolean isInternetConnection()
    {

        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return  connected;
    }


}