package com.example.emploi;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Exam extends Fragment {

    DbHelper db ;
    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    //list of courses
    public List<Cours> listCours = new ArrayList<>();
    private RecyclerView recyclerView;
    private CoursAdapter mAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    protected View mView;


    public Exam() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters
    public static Exam newInstance(String param1, String param2) {
        Exam fragment = new Exam();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        db = new DbHelper(getContext());
        View view = inflater.inflate(R.layout.exams, container, false);
        this.mView = view;


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int year = calendar.get(java.util.Calendar.YEAR);
        int month = calendar.get(java.util.Calendar.MONTH);
        int dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);
        ArrayList<String> grps = getGrpsSharedPrefs();
        try {
            listCours = db.getNextEvaluation(grps);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        mAdapter = new CoursAdapter(listCours,"");
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        System.out.print(listCours.toString());
        return  view;

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public ArrayList<String> getGrpsSharedPrefs(){
        ArrayList<String> grps = new ArrayList<String>();

        SharedPreferences prefs = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String td1 = prefs.getString("td1_selected", null);

        String td2 = prefs.getString("td2_selected", null);

        String td3 = prefs.getString("td3_selected", null);

        String td4 = prefs.getString("td4_selected", null);

        String td5 = prefs.getString("td5_selected", null);

        String td6 = prefs.getString("td6_selected", null);

        String td7 = prefs.getString("td7_selected", null);

        String td8 = prefs.getString("td8_selected", null);

        String td9 = prefs.getString("td9_selected", null);

        grps.add(td1);
        grps.add(td2);
        grps.add(td3);
        grps.add(td4);
        grps.add(td5);
        grps.add(td6);
        grps.add(td7);
        grps.add(td8);
        grps.add(td9);

        return grps;
    }



}
