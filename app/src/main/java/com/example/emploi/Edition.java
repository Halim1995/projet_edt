package com.example.emploi;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Edition.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Edition#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Edition extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextInputEditText urlInput;

    CheckBox groupe1;
    CheckBox groupe2;
    CheckBox groupe3;
    CheckBox groupe4;
    CheckBox groupe5;
    CheckBox groupe6;
    CheckBox groupe7;
    CheckBox groupe8;
    CheckBox groupe9;
    Button save;
    public boolean isFirstLunch() {
        return isFirstLunch;
    }

    public void setFirstLunch(boolean firstLunch) {
        isFirstLunch = firstLunch;
    }

    private boolean isFirstLunch = false;
    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public Edition() {
        // Required empty public constructor

    }


    // TODO: Rename and change types and number of parameters
    public static Edition newInstance(String param1, String param2) {
        Edition fragment = new Edition();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.edition, container, false);
        urlInput = view.findViewById(R.id.url_edt);
        groupe1 = view.findViewById(R.id.gr1);
        groupe2 = view.findViewById(R.id.gr2);
        groupe3 = view.findViewById(R.id.gr3);
        groupe4 = view.findViewById(R.id.gr4);
        groupe5 = view.findViewById(R.id.gr5);
        groupe6 = view.findViewById(R.id.gr6);
        groupe7 = view.findViewById(R.id.gr7);
        groupe8 = view.findViewById(R.id.gr8);
        groupe9 = view.findViewById(R.id.gr9);

        //set cheked buttons
        if(!isFirstLunch) {
            getGrpsSharedPrefs();
        }
        save = (Button) view.findViewById(R.id.save_pref);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE).edit();


                //save configuration action
                boolean td1_selected = groupe1.isChecked();
                boolean td2_selected = groupe2.isChecked();
                boolean td3_selected = groupe3.isChecked();
                boolean td4_selected = groupe4.isChecked();
                boolean td5_selected = groupe5.isChecked();
                boolean td6_selected = groupe6.isChecked();
                boolean td7_selected = groupe7.isChecked();
                boolean td8_selected = groupe8.isChecked();
                boolean td9_selected = groupe9.isChecked();


                if(td1_selected){
                    editor.putString("td1_selected", "L3info_td1");
                } else editor.putString("td1_selected", "Word_does_not_exist_in_data");


                if(td2_selected){
                    editor.putString("td2_selected", "L3info_td2");
                }else editor.putString("td2_selected", "Word_does_not_exist_in_data");

                if(td3_selected){
                    editor.putString("td3_selected", "L3info_td3");
                }else editor.putString("td3_selected", "Word_does_not_exist_in_data");

                if(td4_selected){
                    editor.putString("td4_selected", "L3info_td4");
                }else editor.putString("td4_selected", "Word_does_not_exist_in_data");

                if(td5_selected){
                    //td5 alt
                    editor.putString("td5_selected", "L3info_td5 alt");
                }else editor.putString("td5_selected", "Word_does_not_exist_in_data");

                if(td6_selected){
                    //td6 alt
                    editor.putString("td6_selected", "L3info_td6 alt");
                }else editor.putString("td6_selected", "Word_does_not_exist_in_data");

                if(td7_selected){
                    //projet programmation
                    editor.putString("td7_selected", "ue projet programmation 2");
                }else editor.putString("td7_selected", "Word_does_not_exist_in_data");

                if(td8_selected){
                    //projet robot
                    editor.putString("td8_selected", "ue projet robot2");
                }else editor.putString("td8_selected", "Word_does_not_exist_in_data");

                if(td9_selected){
                    //projet syr
                    editor.putString("td9_selected", "ue projet syr 2");
                }else editor.putString("td9_selected", "Word_does_not_exist_in_data");

                editor.putString("url_edt",urlInput.getText().toString());

                //set first lunch to false
                editor.putString("first_lance", "false");
                editor.apply();

                CharSequence text = "Les modifications ont bien été enregistrées !";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getContext(), text, duration);
                toast.show();



            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Edition.OnFragmentInteractionListener) {
            mListener = (Edition.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }



    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getGrpsSharedPrefs(){
        SharedPreferences prefs = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String td1 = prefs.getString("td1_selected", null);

        String td2 = prefs.getString("td2_selected", null);

        String td3 = prefs.getString("td3_selected", null);

        String td4 = prefs.getString("td4_selected", null);

        String td5 = prefs.getString("td5_selected", null);

        String td6 = prefs.getString("td6_selected", null);

        String td7 = prefs.getString("td7_selected", null);

        String td8 = prefs.getString("td8_selected", null);

        String td9 = prefs.getString("td9_selected", null);

        if(!td1.equals("Word_does_not_exist_in_data"))
          groupe1.setChecked(true);
        if(!td2.equals("Word_does_not_exist_in_data"))
            groupe2.setChecked(true);
        if(!td3.equals("Word_does_not_exist_in_data"))
            groupe3.setChecked(true);
        if(!td4.equals("Word_does_not_exist_in_data"))
            groupe4.setChecked(true);
        if(!td5.equals("Word_does_not_exist_in_data"))
            groupe5.setChecked(true);
        if(!td6.equals("Word_does_not_exist_in_data"))
            groupe6.setChecked(true);
        if(!td7.equals("Word_does_not_exist_in_data"))
            groupe7.setChecked(true);
        if(!td8.equals("Word_does_not_exist_in_data"))
            groupe8.setChecked(true);
        if(!td6.equals("Word_does_not_exist_in_data"))
            groupe9.setChecked(true);

        String url_saved = prefs.getString("url_edt", "Default");
        urlInput.setText(url_saved);
    }


}
