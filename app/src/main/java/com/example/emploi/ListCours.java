package com.example.emploi;


import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListCours extends Fragment  {
    DbHelper db ;
    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    //list of courses
    public List<Cours> listCours = new ArrayList<>();

    TextView CcourseStart;
    TextView CcourseEnd;
    TextView description;
    TextView SecondNextCourse;
    TextView hRest;
    TextView minRest;
    TextView jReste;
    //TextView sReste;
    TextView courseHasStarted;
    Button refresh ;
    Spinner spinner;

    public ListCours() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        db = new DbHelper(getContext());
        View view = inflater.inflate(R.layout.list_cours, container, false);

        CcourseStart = (TextView) view.findViewById(R.id.start);
        CcourseEnd = (TextView) view.findViewById(R.id.end);
        description = (TextView) view.findViewById(R.id.next_course);
        SecondNextCourse = (TextView) view.findViewById(R.id.second_next_course);
        hRest = (TextView) view.findViewById(R.id.h_rest);
        minRest = (TextView) view.findViewById(R.id.m_rest);
        jReste = (TextView) view.findViewById(R.id.j_rest);
        //sReste = (TextView) view.findViewById(R.id.s_rest);
        courseHasStarted = (TextView) view.findViewById(R.id.already_start_msg);
        refresh = (Button) view.findViewById(R.id.refresh);



        String[] arraySpinner = new String[] {
                "1", "2", "3", "4", "5", "6", "7"
        };


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextCourseView();

            }
        });


        //executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/tdoption/3390,3391,3392,3393,3394,3395,28400,28399,28401");
        nextCourseView();
        return view;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    public void nextCourseView(){
        ArrayList<String> grps;
        grps = getGrpsSharedPrefs();
        try {
            listCours = db.getNextCourse(grps);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormatHourAndM = new SimpleDateFormat(
                "HH:mm");
        dateFormatHourAndM.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date DTStart = listCours.get(0).getDTSTART();
        Date DTEnd = listCours.get(0).getDTEND();

        String hourStart = dateFormatHourAndM.format(DTStart);
        String hourEnd = dateFormatHourAndM.format(DTEnd);

        System.out.print(listCours.toString());
        CcourseStart.setText(hourStart);
        CcourseEnd.setText(hourEnd);
        description.setText(listCours.get(0).getDESCRIPTION());
        SecondNextCourse.setText(listCours.get(1).getSUMMARRY());

        long milliseconds = listCours.get(0).getDTSTART().getTime() -  java.util.Calendar.getInstance().getTime().getTime() ;


        //convert the millisec to day, hour and sec
        final long dy = TimeUnit.MILLISECONDS.toDays(milliseconds);
        final long hr = TimeUnit.MILLISECONDS.toHours(milliseconds)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));
        final long min = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));

                    hRest.setText("" + (hr -2));
                    minRest.setText("" + min);
                    jReste.setText("" + dy);
                    //sReste.setText("" + sec);
        if(min<=0)
            courseHasStarted.setVisibility(View.VISIBLE);
    }

    public ArrayList<String> getGrpsSharedPrefs(){
        ArrayList<String> grps = new ArrayList<String>();

        SharedPreferences prefs = getContext().getSharedPreferences("grp_td", MainActivity.MODE_PRIVATE);

        String td1 = prefs.getString("td1_selected", null);

        String td2 = prefs.getString("td2_selected", null);

        String td3 = prefs.getString("td3_selected", null);

        String td4 = prefs.getString("td4_selected", null);

        String td5 = prefs.getString("td5_selected", null);

        String td6 = prefs.getString("td6_selected", null);

        String td7 = prefs.getString("td7_selected", null);

        String td8 = prefs.getString("td8_selected", null);

        String td9 = prefs.getString("td9_selected", null);

        grps.add(td1);
        grps.add(td2);
        grps.add(td3);
        grps.add(td4);
        grps.add(td5);
        grps.add(td6);
        grps.add(td7);
        grps.add(td8);
        grps.add(td9);

        return grps;
    }


}
