package com.example.emploi;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class CoursAdapter extends RecyclerView.Adapter<CoursAdapter.MyViewHolder>{

    private List<Cours> coursList;
    private String type;

    SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyyMMdd'T'HHmmss'Z'");
    SimpleDateFormat dateFormatHourAndM = new SimpleDateFormat(
            "HH:mm");


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView description;
        public TextView timeStart;
        public TextView timeEnd;
        public TextView course_info_status;

        public MyViewHolder(View view) {
            super(view);
            description = (TextView) view.findViewById(R.id.description);
            timeStart = (TextView) view.findViewById(R.id.timeStart);
            timeEnd = (TextView) view.findViewById(R.id.timeEnd);
            course_info_status = (TextView) view.findViewById(R.id.course_type);

        }
    }


    public CoursAdapter(List<Cours> coursList, String type) {
        this.coursList = coursList;
        this.type = type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormatHourAndM.setTimeZone(TimeZone.getTimeZone("UTC"));

        Cours cours = coursList.get(position);
        Date DTStart = cours.getDTSTART();
        Date DTEnd = cours.getDTEND();

        String hourStart = dateFormatHourAndM.format(DTStart);
        String hourEnd = dateFormatHourAndM.format(DTEnd);

        holder.description.setText(cours.getDESCRIPTION());
        holder.timeStart.setText(hourStart);
        holder.timeEnd.setText(hourEnd);
        String description  = cours.getDESCRIPTION();
        System.out.print("");
        String courseInfos = "";
        if(cours.getDESCRIPTION().indexOf("Evaluation")!=-1 && !type.equals("courses")) {
            try {
                courseInfos = courseInfos + formatdateToString(DTStart);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(cours.getDESCRIPTION().indexOf("Annulation")!=-1){
            courseInfos =courseInfos + "Annulé";
            holder.course_info_status.setTextColor(Color.RED);

        }

        holder.course_info_status.setText(courseInfos);
        }

    @Override
    public int getItemCount() {
        return coursList.size();
    }

    public Date StringToDate(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyyMMdd'T'HHmmss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();

        try {
            date = dateFormat.parse(dateString);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  date;
    }

    public String formatdateToString(Date date) throws ParseException {

        String yyyy;
        String MM;
        String dd;
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH)+1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String dateStingTotal;
        yyyy ="" + (date.getYear()+1900);
        if(month<10)
            MM ="0" + month;
        else
            MM = "" + month;
        if(day<10)
            dd ="0" +day;
        else
            dd = "" + day;
        dateStingTotal =""+ yyyy + "-" + MM + "-" + dd;
        return dateStingTotal;
    }
}
